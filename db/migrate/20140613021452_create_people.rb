class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :birthday
      t.string :name
      t.string :search_url
      t.string :image_url
      t.integer :hits

      t.timestamps
    end
  end
end
