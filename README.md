### Dependencies ###
* ember-cli
* rails
* postgres
* facebook app with you as dev (deployed site should work [birthday-battle @ heroku](http://birthday-battle.herokuapp.com/birthday)
### How do I get set up? ###

#### rails ####

in /birthday-battle...
```
#!bash

bundle install
bundle exec rake db:create db:migrate (I think)
rails s
```
### ember ###
in /birthday-battle/birthday-battle-ember
```
#!bash

npm install && bower install
ember s --proxy http://localhost:3000
```
## Major Issues ##
### Very long request times ###
birthday-battles currently works off of web scraping :( which currently happens during the web request. Patience is usually rewarded, timeouts are pretty rare right now.
### Facebook logins ###
Will be out of sorts locally since I'm the owner of the local facebook app. Deployed site should work with others' login: [birthday-battle @ heroku](http://birthday-battle.herokuapp.com/birthday).

## Next features ##
Modal dialog declaring the winner!??