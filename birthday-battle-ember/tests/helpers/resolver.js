import Resolver from 'ember/resolver';

var resolver = Resolver.create();

resolver.namespace = {
  modulePrefix: 'birthday-battle-ember'
};

export default resolver;
