export default {
  name: 'facebook',
  initialize: function(container) {
    window.fbAsyncInit = function() {
      FB.init({
        appId  : window.FACEBOOK_APP_ID,
        status : true,
        cookie : true,
        xfbml  : false
      });

      FB.getLoginStatus(function(response) {
        if (response.status !== 'connected') { return; }

        var controller = container.lookup('controller:application');
        controller.set("user", {});

        FB.api("/me", function (user_res) {
          if (user_res && !user_res.error) {
            FB.api("/me/picture", { "type": "large" },
              function (response) {
              if (response && !response.error){
                user_res.img = response.data.url;
                controller.set("user", user_res);
              }
            });
          }
        });
      });
    };

    var id = 'facebook-jssdk';
    if (document.getElementById(id)) {
     return;
    }

    var first_script = document.getElementsByTagName('script')[0];
    var js = document.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    first_script.parentNode.insertBefore(js, first_script);
  }
};