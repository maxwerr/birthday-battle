import Ember from 'ember';

var application = Ember.Controller.extend({

  actions: {

    login: function() {

      var _this = this;

      FB.getLoginStatus(function(response) {
        if (response.status === 'connected'){
          _this.send('setUserFromApi');
        }
        else {
          FB.login(function(response) {
            if (response.authResponse) {
              _this.send('setUserFromApi');
            }
          }, {scope: "user_birthday, public_profile"});
        }
      });
    },

    logout: function() {
      var that = this;
      FB.api('/me/permissions', 'DELETE', function() {
        that.set("user", null);
      });
    },

    setUserFromApi: function(){
      var ctrl = this;
      FB.api("/me", function (user_res) {
        if (user_res && !user_res.error) {
          FB.api("/me/picture", { "type": "large" }, function (response) {
            if (response && !response.error){
              user_res.img = response.data.url;
              ctrl.set("user", user_res);
            }
          });
        }
      });
    }
  }
});

export default application;