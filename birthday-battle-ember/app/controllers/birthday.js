import Ember from 'ember';

var birthday = Ember.Controller.extend({

  needs: ["application"],

  playDisabled: function(){
    var user = this.get('controllers.application.user');
    return (user == null) || (user.first_name == null);
  }.property('controllers.application.user'),

  birthday: function(){
    return this.get('controllers.application.user.birthday');
  }.property('controllers.application.user.birthday'),

  actions: {
    getBirthday: function(){
      var birthday = this.get('controllers.application.user').birthday;
      this.set('month', parseInt(birthday.split("/")[0],10));
      this.set('day', parseInt(birthday.split("/")[1],10));

      var that = this;
      this.set('loading', true);

      this.store.find('person', 
      {
        limit: 10, 
        month: this.get('month'), 
        day: this.get('day')
      }).then(
      function(response){
        var ppl = response.content;
        var sum = 0;
        for(var i = 0; i < ppl.length; i++) {
          sum += ppl[i].get('hits');
        }
        that.set('facebook_score', ((sum / ppl.length)/1000000).toFixed(2));
        that.set('birthday_people', response.content);
        that.set('loading', false);
      });
    },
  }
});

export default birthday;