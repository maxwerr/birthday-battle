import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    return this.store.find('person', { limit: 10, day: dd, month: mm });
  },
  setupController: function(controller, model) {
    controller.set('model', model);

    var ppl_arr = model.get('content');
    var sum_hits = 0;
    for(var i = 0; i < ppl_arr.length; i++) {
      sum_hits += ppl_arr[i].get('hits');
    }

    controller.set('today_score', (sum_hits / ppl_arr.length / 1000000).toFixed(2));
  }
});