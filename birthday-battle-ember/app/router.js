import Ember from 'ember';

var Router = Ember.Router.extend({
  location: BirthdayBattleEmberENV.locationType
});

Router.map(function() {
  this.route('birthday');
  this.route('privacy');
});

export default Router;
