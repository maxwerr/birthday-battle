import DS from 'ember-data';

var Person = DS.Model.extend({
  name: DS.attr('string'),
  search_url: DS.attr('string'),
  image_url: DS.attr('string'),
  hits: DS.attr('number'),
  birthday: DS.attr('string'),
  
  pretty_hits: function() {
    return String(this.get('hits')/1000000) + "m";
  }.property('hits')
});

export default Person;