class PersonSerializer < ActiveModel::Serializer
  attributes :id, :birthday, :name, :search_url, :image_url, :hits
end
