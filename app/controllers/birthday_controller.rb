class BirthdayController < ApplicationController

  def show
    month = params[:month]
    day = params[:day]

    ppl = Person.where(birthday: "#{month.to_i} - #{day.to_i}").to_a  # 9 - 13

    if ppl.size == 0

      puts "Beginning scraping for #{month}/#{day}"

      born_scraper = BornTodayWorker.new
      fame_scraper = FamousBirthdaysWorker.new

      ppl.concat born_scraper.people_born_on(month, day)
      ppl.concat fame_scraper.people_born_on(month, day)

      # prefer the fame_scraper in dupe names
      ppl_group = ppl.group_by { |p| p.name[0..2] + p.name[-3..-1] }
      ppl = ppl_group.map do |name_hash, persons| 
        if persons.size == 1
          persons.first
        else
          persons[1]
        end
      end

      ppl.sort! { |b,a| a.hits <=> b.hits }
      ppl.each { |p| p.save! }
    end

    if params[:limit]
      ppl = ppl.take params[:limit].to_i
    end

    render json: ppl, root: "people"
  end
end
