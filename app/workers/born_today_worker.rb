require 'rubygems'
require 'nokogiri'
require 'open-uri'

class BornTodayWorker
  HOSTNAME = 'http://www.born-today.com'
  NO_IMAGE_URL = "http://deborahjones.theworldrace.org/blogphotos/theworldrace/deborahjones/no-1.jpg"
 
  def people_born_on month, day
    ppl = []
    page = Nokogiri::HTML(open(url_for_date month, day))
    page.xpath("//div[@class='item']").each do |item|
      person = item_to_person item
      if person
        person.birthday = "#{month} - #{day}" # 9 - 13
        ppl << person
      end
    end

    ppl
  end

  private

  def url_for_date month, day
    day = ("0" + day.to_s).split(//).last(2).join
    month = ("0" + month.to_s).split(//).last(2).join
    "#{HOSTNAME}/Today/#{month}-#{day}.htm" # 09-13
  end

  def item_to_person div_item

    img_src = if div_item.css("img").size < 1
                NO_IMAGE_URL
              else
                HOSTNAME + div_item.css("img").attribute("src").value[2..-1]
              end

    person = Person.new
    person.name = div_item.css("a.auth").text
    person.search_url = div_item.css("a.auth").attribute("href").value
    person.image_url = img_src
    person.hits = get_no_hits person.search_url
    person unless person.name.size < 6 || person.hits == 0
  end

  def get_no_hits google_search_url
    begin
      page = Nokogiri::HTML(open(google_search_url.split(" ").join("+")))
    rescue URI::InvalidURIError => e
      return 0
    end

    val = page.css("div#resultStats").text
    val.slice!("About ")
    val.slice!(" results")
    val.slice!(" result")
    val.split(",").join.to_i
  end
end