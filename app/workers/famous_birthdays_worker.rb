require 'rubygems'
require 'nokogiri'
require 'open-uri'

class FamousBirthdaysWorker
  HOSTNAME = 'http://www.famousbirthdays.com'
 
  def people_born_on month, day
    ppl = []
    page = Nokogiri::HTML(open(url_for_date month, day))
    page.css("section.celebs-box").to_a[0].css("a").each do |item|
      person = item_to_person item
      if person
        person.birthday = "#{month} - #{day}" # 9 - 4
        ppl << person
      end
    end

    ppl
  end

  private

  def url_for_date month, day
    month = Date::MONTHNAMES[month.to_i].downcase
    "#{HOSTNAME}/#{month}#{day.to_i}.html" # september4.html
  end

  def item_to_person item
    person = Person.new
    person.name = item.css("figure > img").first.attributes['alt'].value
    person.search_url = "https://www.google.com/search?q=" + person.name.split.join("+")
    person.image_url = item.css("figure > img").first.attributes['src'].value
    person.hits = get_no_hits person.search_url
    person unless person.name.size < 6
  end

  def get_no_hits google_search_url
    begin
      page = Nokogiri::HTML(open(google_search_url.split(" ").join("+")))
    rescue URI::InvalidURIError => e
      return 0
    end

    val = page.css("div#resultStats").text
    val.slice!("About ")
    val.slice!(" results")
    val.slice!(" result")
    val.split(",").join.to_i
  end
end